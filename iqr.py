
import plotly.graph_objects as px
def Median(l):
    if len(l) % 2 == 1:
        # If odd
        return l[len(l)//2]
    else:
        # If even
        return (l[len(l)//2-1] + l[len(l)//2])/2

def Quartiles(l):
    if len(l) % 2 != 1:
        q1list = l[:len(l)//2]
        q3list = l[len(l)//2:]

    else:
        medianplace = (len(l) + 1) / 2
        q1list = l[:int(medianplace -1)]
        q3list = l[-int(medianplace -1):]
    
    q1 = Median(q1list)
    q3 = Median(q3list)
    returnlist = []
    returnlist.append(q1)
    returnlist.append(q3)
    return returnlist

def main():
    print("Enter a dataset (Seperate by spaces): ")
    try:
        inp = list(map(float, input("> ").split()))
        inp.sort()
        min = inp[0]
        max = inp[-1]
        range = max - min
    
        print("\nData")
        print("----")
        print(f"Q1: {Quartiles(inp)[0]}")
        print(f"Q2 (Median): {Median(inp)}")    
        print(f"Q3: {Quartiles(inp)[1]}")
        print(f"Five Number Summary: {min}, {Quartiles(inp)[0]}, {Median(inp)}, {Quartiles(inp)[1]}, {max}")
        print(f"Range: {range}")
        print(f"Interquartile Range: {Quartiles(inp)[1] - Quartiles(inp)[0]}")
        plot = px.Figure()
 
        plot.add_trace(px.Box(x=[min, Median(inp), max]))
 
        plot.show()
    except ValueError as e:
        #print(f"Error: ensure you are entering in valid numbers.")
        print(e)
if __name__ == "__main__":
    main()
 
 

 
